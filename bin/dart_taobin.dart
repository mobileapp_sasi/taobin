import 'package:dart_taobin/dart_taobin.dart' as dart_taobin;
import 'dart:io';

void main() {
  print('Welcome to TAOBIN');

  print('Please select category');
  Taobin.showCategories();
  int category = int.parse(stdin.readLineSync()!);

  print('Please select menu');
  Taobin.showMenu(category);
  int menu = int.parse(stdin.readLineSync()!);

  print('Please select type');
  Taobin.showType();
  int type = int.parse(stdin.readLineSync()!);
  Taobin.setPrice(type);

  print('Please select sweetness level');
  Taobin.showSweetnessLevel();
  int sweetness = int.parse(stdin.readLineSync()!);

  print('Please select Straw and Lid');
  Taobin.showStrawLid();
  int addStrawLid = int.parse(stdin.readLineSync()!);

  print('\n The menu you ordered :');
  Taobin.showResult(category, menu, type, sweetness, addStrawLid);

  print('');
  Taobin.getMoney();
}

class Taobin {
  static int price = 40;

  static Map<int, String> menuCategories = {1: 'Coffee', 2: 'Tea', 3: 'Milk'};

  static Map<int, String> menuList = new Map();

  static Map<int, String> menuCoffee = {
    1: 'Latte',
    2: 'Espresso',
    3: 'Americano',
    4: 'Cappuccino',
    5: 'Mocha'
  };
  static Map<int, String> menuTea = {
    1: 'Thai',
    2: 'Taiwanese',
    3: 'Matcha',
    4: 'Black',
    5: 'Lime'
  };
  static Map<int, String> menuMilk = {
    1: 'Caramel',
    2: 'Kokuto',
    3: 'Cocoa',
    4: 'Caramel Cocoa',
    5: 'Pink'
  };

  static Map<int, String> menuType = {1: 'Hot', 2: 'Iced', 3: 'Blended'};

  static Map<int, String> sweetnessLevel = {
    1: 'No Sugar',
    2: 'Less Sweet',
    3: 'Normal',
    4: 'Sweet',
    5: 'Very Sweet'
  };

  static Map<int, String> addStrawLid = {
    1: 'Straw',
    2: 'Lid',
    3: 'Straw and Lid'
  };

  static void showCategories() {
    print(menuCategories);
  }

  static void showMenu(int input) {
    switch (input) {
      case 1:
        {
          print(menuCoffee);
          menuList = menuCoffee;
        }
        break;
      case 2:
        {
          print(menuTea);
          menuList = menuTea;
        }
        break;
      case 3:
        {
          print(menuMilk);
          menuList = menuMilk;
        }
        break;
      default:
        {}
        break;
    }
  }

  static void showType() {
    print(menuType);
  }

  static void showSweetnessLevel() {
    print(sweetnessLevel);
  }

  static void showStrawLid() {
    print(addStrawLid);
  }

  static int setPrice(int type) {
    switch (type) {
      case 1:
        {
          price = price;
        }
        break;
      case 2:
        {
          price = price + 5;
        }
        break;
      case 3:
        {
          price = price + 10;
        }
        break;
      default:
        {}
    }
    return price;
  }

  static int getPrice() {
    return price;
  }

  static void showResult(
      int categories, int menu, int type, int sweet, int strLid) {
    var category = menuCategories[categories];
    var menus = menuList[menu];
    var types = menuType[type];
    var sweetness = sweetnessLevel[sweet];
    var strawLid = addStrawLid[strLid];
    print('$types $menus $category $sweetness with $strawLid $price฿');
  }

  static void getMoney() {
    print("Please input your money");
    int money = int.parse(stdin.readLineSync()!);
    price = price - money;
    int remain = price;
    if (remain > 0) {
      int remain = price;
      print("Your input money : $money");
      print("Remaining : $remain");
      getMoney();
    } else if (remain < 0) {
      int exchange = remain - remain - remain;
      print("Exchange : $exchange");
      print("Please wait for your ordered, Thank you");
    } else {
      print("Please wait for your ordered, Thank you");
    }
  }
}
